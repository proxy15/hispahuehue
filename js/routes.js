angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  
        
    .state('hispaMenu.hispa', {
      url: '/Hispa',
      views: {
        'side-menu22': {
          templateUrl: 'templates/hispa.html'
        }
      }
    })
        
      
    .state('hispaMenu.general', {
      url: '/general',
      views: {
        'side-menu22': {
          templateUrl: 'templates/general.html'
        }
      }
    })
      
    .state('hispaMenu', {
      url: '/side-hispa',
      abstract:true,
      templateUrl: 'templates/hispaMenu.html'
    })
      
    
        
    .state('responderPost', {
      url: '/NuevoPGeneral',
      templateUrl: 'templates/responderPost.html'
    })
        
      
    
      
        
    .state('nuevoPost', {
      url: '/NuevoPost',
      templateUrl: 'templates/nuevoPost.html'
    })
        
      
    
      
        
    .state('regionales', {
      url: '/regionales',
      templateUrl: 'templates/regionales.html'
    })
        
      
    ;

  // if none of the above states are matched, use this as the fallback
  
  $urlRouterProvider.otherwise('/side-hispa/Hispa');
  

  

});